package domain;

public class Contract {

	private String contractKind;
	private String year;
	private String quotaType;
	private String amount;
	
	public String getContractKind() {
		return contractKind;
	}
	public void setContractKind(String contractKind) {
		this.contractKind = contractKind;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getQuotaType() {
		return quotaType;
	}
	public void setQuotaType(String quotaType) {
		this.quotaType = quotaType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
