package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import calculator.CountPracaBrutto;
import calculator.CountPracaNetto;
import domain.Contract;

@WebServlet("/main")
public class PracaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = ((HttpServletRequest) request).getSession();
		
		Contract contract = new Contract();
		contract.setContractKind(request.getParameter("contractKind"));
		contract.setYear(request.getParameter("setYear"));
		contract.setQuotaType(request.getParameter("quotaType"));
		contract.setAmount(request.getParameter("amount"));
		
		session.setAttribute("contr",contract);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if(request.getParameter("quotaType").equals("brutto")){
			CountPracaBrutto count=new CountPracaBrutto(Double.parseDouble(request.getParameter("amount")));
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getParameter("contractKind")+"!<br>"+
					"dla roku "+request.getParameter("year")+"<br>"+
					"<table border='1'>"+
					"<tr><td></td><td>Brutto</td><td>"+
					"emerytalne</td><td>rentowe</td><td>"+
					"chorobowe</td><td>zdrowotne</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>netto</td></tr>");
			for(int i=0;i<12;i++){
			out.println("<tr><td>"+ count.getMonths(i)+"</td><td>" + count.getBrutto() +"</td><td>"+count.getEmerytalna()+"</td><td>"+count.getRentowa()+"</td><td>"+count.getChorobowe()+"</td><td>"+count.getZdrowotne()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZaliczkaPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			}
			out.println("<tr><td>Suma</td><td>" + 12*(count.getBrutto()) +
					"</td><td>"+12*(count.getEmerytalna())+
					"</td><td>"+12*(count.getRentowa())+
					"</td><td>"+12*(count.getChorobowe())+
					"</td><td>"+12*(count.getZdrowotne())+
					"</td><td>"+12*(count.getPodstawa())+
					"</td><td>"+12*(count.getZaliczkaPITdoZaplaty())+
					"</td><td>"+12*(count.getNetto())+
					"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
		else if(request.getParameter("quotaType").equals("netto")){
			CountPracaNetto count=new CountPracaNetto(Double.parseDouble(request.getParameter("amount")));
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getParameter("contractKind")+"!<br>"+
					"dla roku "+request.getParameter("year")+"<br>"+
					"<table border='1'>"+
					"<tr><td></td><td>Brutto</td><td>"+
					"emerytalne</td><td>rentowe</td><td>"+
					"chorobowe</td><td>zdrowotne</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>netto</td></tr>");
			for(int i=0;i<12;i++){
				out.println("<tr><td>"+ count.getMonths(i)+"</td><td>" + count.getBrutto() +"</td><td>"
						+count.getEmerytalna()+"</td><td>"+count.getRentowa()+"</td><td>"+count.getChorobowe()+"</td><td>"
						+count.getZdrowotne()+"</td><td>"+count.getPodstawa()+"</td><td>"
						+count.getZaliczkaPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			}
			out.println("<tr><td>Suma</td><td>" + 12*(count.getBrutto()) +
					"</td><td>"+12*(count.getEmerytalna())+
					"</td><td>"+12*(count.getRentowa())+
					"</td><td>"+12*(count.getChorobowe())+
					"</td><td>"+12*(count.getZdrowotne())+
					"</td><td>"+12*(count.getPodstawa())+
					"</td><td>"+12*(count.getZaliczkaPITdoZaplaty())+
					"</td><td>"+12*(count.getNetto())+
					"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
	}
}