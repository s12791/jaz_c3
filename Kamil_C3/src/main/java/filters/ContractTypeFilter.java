package filters;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter({"/main"})
public class ContractTypeFilter implements Filter{

	FilterConfig filterConfig = null;

	public void init(FilterConfig config) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		
		response.setContentType("text/html");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		PrintWriter out = resp.getWriter();

		req.getSession().setAttribute("contractKind", req.getParameter("contractKind").toString());
		req.getSession().setAttribute("year", req.getParameter("year").toString());
		req.getSession().setAttribute("quotaType", req.getParameter("quotaType").toString());
		req.getSession().setAttribute("amount", req.getParameter("amount").toString());
		
		if (req.getParameter("contractKind").equals("praca"))chain.doFilter(req, resp);
		else if (req.getParameter("contractKind").equals("dzielo"))resp.sendRedirect("dzielo");
		else if (req.getParameter("contractKind").equals("zlecenie"))resp.sendRedirect("zlecenie");
		out.close();
	}
	public void destroy() {}
}
